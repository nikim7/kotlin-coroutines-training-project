package com.training.charmas.app.backend

import com.squareup.moshi.Json
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface DemoApi {

    @GET("/posts")
    suspend fun getPosts(): List<Post>

    @GET("/posts/{postId}/comments")
    suspend fun getComments(@Path("postId") id: Long): List<Comment>

    @GET("/users/{userId}")
    suspend fun getUser(@Path("userId") userId: Long): User
}

fun getDemoApi(): DemoApi = Retrofit.Builder()
    .baseUrl("https://jsonplaceholder.typicode.com")
    .client(httpClient())
    .addConverterFactory(MoshiConverterFactory.create())
    .build()
    .create(DemoApi::class.java)

private fun httpClient() = OkHttpClient
    .Builder()
    .addNetworkInterceptor(HttpLoggingInterceptor().apply { level = Level.BASIC })
    .addInterceptor(object : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {
            Thread.sleep(100)
            return chain.proceed(chain.request())
        }
    })
    .build()

data class Post(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "userId") val userId: Long,
    @field:Json(name = "title") val title: String,
    @field:Json(name = "body") val body: String
)

data class Comment(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "name") val name: String,
    @field:Json(name = "email") val email: String,
    @field:Json(name = "body") val body: String
)

data class User(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "name") val name: String
)

data class Album(
    @field:Json(name = "id") val id: Long,
    @field:Json(name = "userId") val userId: Long,
    @field:Json(name = "title") val title: String
)
