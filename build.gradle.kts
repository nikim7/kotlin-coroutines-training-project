plugins {
    kotlin("jvm") version "1.3.61"
}

group = "com.training"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))

    implementation("com.squareup.retrofit2:retrofit:2.6.1")
    implementation("com.squareup.retrofit2:converter-moshi:2.5.0")
    implementation("com.squareup.okhttp3:okhttp:4.1.0")
    implementation("com.squareup.okhttp3:logging-interceptor:4.1.0")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-jdk8:1.3.3")

    testImplementation("org.junit.jupiter:junit-jupiter:5.6.0")
}

tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
    }
}

tasks.test {
    useJUnitPlatform()
}
